<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>EAW</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="assets/img/favicon.png" rel="icon">
        <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.gstatic.com" rel="preconnect">
        <link
            href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
            rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
        <link href="assets/vendor/quill/quill.snow.css" rel="stylesheet">
        <link href="assets/vendor/quill/quill.bubble.css" rel="stylesheet">
        <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
        <link href="assets/vendor/simple-datatables/style.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/menu.css"/>
        


        <!-- =======================================================
        * Template Name: NiceAdmin - v2.5.0
        * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
        <style>

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid transparent;
                border-radius: .25rem;
                margin-bottom: 1.5rem;
                box-shadow: 0 2px 6px 0 rgb(218 218 253 / 65%), 0 2px 6px 0 rgb(206 206 238 / 54%);
            }
            *{
                margin: 0;
                padding: 0;
                box-sizing: border-box;
                font-family: 'Poppins',sans-serif;
                overflow: auto;
            }
            body{
               
                overflow: hidden;
            }
            ::selection{
                background: rgba(26,188,156,0.3);
            }
            .container2{
                max-width: 440px;
                
                padding: 0 5px;
                margin: 20px auto;
            }
            .wrapper{
                width: 100%;
                background: #fff;
                border-radius: 5px;
                box-shadow: 0px 4px 10px 1px rgba(0,0,0,0.2);
            }
            .wrapper .title{
                height: 90px;
                background: #16a085;
                border-radius: 5px 5px 0 0;
                color: #fff;
                font-size: 30px;
                font-weight: 600;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .wrapper form{
                padding: 5px 25px 5px 25px;
            }
            .wrapper form .row{
                height: 45px;
                margin-bottom: 15px;
                position: relative;
            }
            .wrapper form .row input{
                height: 100%;
                width: 100%;
                outline: none;
                padding-left: 60px;
                border-radius: 5px;
                border: 1px solid lightgrey;
                font-size: 16px;
                transition: all 0.3s ease;
            }
            form .row input:focus{
                border-color: #16a085;
                box-shadow: inset 0px 0px 2px 2px rgba(26,188,156,0.25);
            }
            form .row input::placeholder{
                color: #999;
            }
            .wrapper form .row i{
                position: absolute;
                width: 47px;
                height: 100%;
                color: #fff;
                font-size: 18px;
                background: #16a085;
                border: 1px solid #16a085;
                border-radius: 5px 0 0 5px;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .wrapper form .pass{
                margin: -8px 0 20px 0;
            }
            .wrapper form .pass a{
                color: #16a085;
                font-size: 17px;
                text-decoration: none;
            }
            .wrapper form .pass a:hover{
                text-decoration: underline;
            }
            .wrapper form .button input{
                color: #fff;
                font-size: 20px;
                font-weight: 500;
                padding-left: 0px;
                background: #16a085;
                border: 1px solid #16a085;
                cursor: pointer;
            }
            form .button input:hover{
                background: #12876f;
            }
            .wrapper form .logins{
                text-align: center;
                margin-top: 20px;
                font-size: 17px;
            }
            .wrapper form .logins a{
                color: #16a085;
                text-decoration: none;
            }
            form .logins a:hover{
                text-decoration: underline;
            }

        </style>
    </head>

    <body>

        <!-- ======= Header ======= -->
        <header id="header" class="header fixed-top d-flex align-items-center">

            <div class="d-flex align-items-center justify-content-between">
                <a href="index.html" class="logo d-flex align-items-center">
                    <img src="assets/img/logo.png" alt="">
                    <span class="d-none d-lg-block">EAW</span>
                </a>
                <i class="bi bi-list toggle-sidebar-btn"></i>
            </div><!-- End Logo -->

            <div class="search-bar">
                <form class="search-form d-flex align-items-center" method="POST" action="#">
                    <input type="text" name="query" placeholder="Search" title="Enter search keyword">
                    <button type="submit" title="Search"><i class="bi bi-search"></i></button>
                </form>
            </div><!-- End Search Bar -->

            <nav class="header-nav ms-auto">
                <ul class="d-flex align-items-center">

                    <li class="nav-item d-block d-lg-none">
                        <a class="nav-link nav-icon search-bar-toggle " href="#">
                            <i class="bi bi-search"></i>
                        </a>
                    </li><!-- End Search Icon-->

                    <section class="row flex">

                        <ul class="header__top-list">             
                            <c:if test="${sessionScope.acc == null}">
                                <li class="header__top-item">
                                    <a href="login.jsp" class="header__top-link">Đăng nhập</a>
                                </li>
                            </c:if>
                            <c:if test="${sessionScope.acc != null}">
                                <li class="header__top-item">
                                    <a href="#" class="header__top-link">Xin chào ${sessionScope.acc.usename}</a>
                                </li>
                                <li class="header__top-item">
                                    <a href="logout" class="header__top-link">Đăng xuất</a>
                                </li>
                            </c:if>
                        </ul>
                    </section>

                </ul>
            </nav><!-- End Icons Navigation -->

        </header><!-- End Header -->

      <div class="container2" style="margin-top: 90px">

            <div class="wrapper">
               

                <form action="newpass" method="post">

                    <h5 style="color: red; margin-bottom: 30px">${requestScope.err}</h5>
                     <p>Enter Old Password:</p>
                    <div class="row">

                        <i class="fas fa-lock"></i>
                        <input type="password" name="oldpass" placeholder="Old Password" required/><br/>
                    </div>
                    <p>Enter New Password:</p>
                    <div class="row">

                        <i class="fas fa-lock"></i>
                        <input type="password" name="pass" placeholder="New Password" required/><br/>
                    </div>
                    <p>Repass:</p>
                    <div class="row">

                        <i class="fas fa-lock"></i>
                        <input type="password" name="repass" placeholder="Repeat Password" required/><br/>
                    </div>
                    <p>Security Code:</p>
                    <input style="border:2px solid;" type="text" name="scode"/><br/>
                    <br/>
                    <div class="row button">
                        <input type="submit" value="Change">
                    </div>

                    <div class="logins">If you forgot password <a href="forgotpass">Change now</a></div>

                </form>   
            </div>


        </div>



        <!-- Vendor JS Files -->
        <script src="assets/vendor/apexcharts/apexcharts.min.js"></script>
        <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/vendor/chart.js/chart.umd.js"></script>
        <script src="assets/vendor/echarts/echarts.min.js"></script>
        <script src="assets/vendor/quill/quill.min.js"></script>
        <script src="assets/vendor/simple-datatables/simple-datatables.js"></script>
        <script src="assets/vendor/tinymce/tinymce.min.js"></script>
        <script src="assets/vendor/php-email-form/validate.js"></script>

        <!-- Template Main JS File -->
        <script src="assets/js/main.js"></script>

    </body>

</html>